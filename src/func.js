const getSum = (str1, str2) => {
  if(isNaN(+str1) || isNaN(+str2)) return false;
  if(typeof str1 !== "string" || typeof str2 !== "string") return false;

  let a = +str1;
  let b = +str2;

  return (a+b).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postsCount = listOfPosts.filter(post => post.author === authorName).length;
  let commentsCount = 0; 
  listOfPosts.forEach(post => {
    if("comments" in post){
      post.comments.forEach(comment=> {
        if(comment.author === authorName) commentsCount++;
      })
    }
  })

  return `Post:${postsCount},comments:${commentsCount}`;
};

const tickets=(people)=> {
  const price = 25;
  let vasyaMoney = 0;

  for (const money of people) {
    let m = parseInt(money); 

    if(m === price) vasyaMoney += money;
    else if(m - price <= vasyaMoney) vasyaMoney += m - price;
    else return "NO";
  }

  return "YES";
  
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
